package main

import (
	"bootcamp/bigint/bigint"
	"fmt"
	// "home/omadbek/uDevs/Go/bigint/bigint"
)

func main() {

	a, err := bigint.NewInt("-20000")
	if err != nil {
		panic(err)
	}
	b, err := bigint.NewInt("123456789")
	if err != nil {
		panic(err)
	}

	err = a.Set("-100")
	if err != nil {
		panic(err)
	}

	c := bigint.Add(a, b)
	d := bigint.Sub(a, b)
	e := bigint.Multiply(a, b)
	f := bigint.Mod(a, b)
	// k := bigint.Abs(a, b)
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	// fmt.Println(k)

}
