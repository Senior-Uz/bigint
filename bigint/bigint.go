package bigint

import (
	"errors"
	"strconv"
	"strings"
)

type Bigint struct {
	Value string
}

var ErrorBadInput = errors.New("bad input, please input only number")

func NewInt(num string) (Bigint, error) {
	allowed := "1234567890"
	var err bool

	if strings.HasPrefix(num, "-") {
		num = strings.Replace(num, "-", "", 1)
	}
	if strings.HasPrefix(num, "0") {
		err = true
	}
	arr := strings.Split(num, "")
	for _, v := range arr {
		if !strings.Contains(allowed, v) {
			err = true
		}
	}

	if err {
		return Bigint{Value: num}, ErrorBadInput
	} else {
		return Bigint{Value: num}, nil

	}
}

func (z *Bigint) Set(num string) error {
	//
	// Validate input
	if _, err := strconv.ParseInt(num, 10, 64); err != nil {
		return errors.New("Wrong input")
	}
	//
	z.Value = num
	return nil
}

func Add(a, b Bigint) Bigint {
	//
	// MATH ADD
	c, _ := strconv.ParseInt(a.Value, 10, 64)
	d, _ := strconv.ParseInt(b.Value, 10, 64)
	sum := c + d
	//
	return Bigint{strconv.FormatInt(int64(sum), 10)}
}

func Sub(a, b Bigint) Bigint {
	//
	// MATH SUB
	e, _ := strconv.ParseInt(a.Value, 10, 64)
	f, _ := strconv.ParseInt(b.Value, 10, 64)
	difference := f - e

	return Bigint{strconv.FormatInt(int64(difference), 10)}
	 

}

func Multiply(a, b Bigint) Bigint {
	//
	g, _ := strconv.ParseInt(a.Value, 10, 64)
	h, _ := strconv.ParseInt(b.Value, 10, 64)
	multiplication := g * h

	return Bigint{strconv.FormatInt(int64(multiplication), 10)}
}

func Mod(a, b Bigint) Bigint {
	//
	i, _ := strconv.ParseInt(a.Value, 10, 64)
	j, _ := strconv.ParseInt(b.Value, 10, 64)
	modulus := i % j
	return Bigint{strconv.FormatInt(int64(modulus), 10)}
	//
 
}


// func (x Bigint) Abs() Bigint {

// // my code here
	   
// }
